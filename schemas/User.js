const {Schema} = require('mongoose')
const uniqueValidator = require('mongoose-unique-validator')

const User = new Schema({
    username: {
        type: String,
        trim: true,
        lowercase: true,
        maxlength: 30,
        minlength: 5,
        require: true,
        unique: true,
        index: true,
    },
    password: {
        type: String,
        trim: true,
        maxlength: 100,
        minlength: 6,
        require: true
    },
    first_name: {
        type: String,
        trim: true,
        required: true,
    },
    last_name: {
        type: String,
        trim: true,
        required: true,
    },
    birthday: {
        type: Date,
        required: true,
    },
    address: {
        type: String,
    },
    identification: {
        type: String,
    },
    email: {
        type: String,
        trim: true,
        lowercase: true,
        maxlength: 100,
        unique: true,
        minlength: 5,
        validate: [(email) => {
            const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            return re.test(email)
        }, "Email is invalid"],
        index: true,
    },
    phone: {
        type: String,
        trim: true,
        maxlength: 20,
        minlength: 8,
        unique: true,
        index: true,
    },
    role: {
        type: String,
        required: true,
        enum: ["renter", "owner", "admin"],
        index: true,
    },
    is_deleted: {
        type: Boolean,
        default: false,
    }
}, {timestamps: true})

User.plugin(uniqueValidator)

module.exports = User
