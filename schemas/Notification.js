const {Schema} = require('mongoose')

const Notification = new Schema({
    receiver: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
        index: true,
    },
    sender: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
        index: true,
    },
    action: {
        type: String,
        required: true,
        trim: true,
        index: true,
    },
    key: {
        type: String,
        trim: true,
        deafult: '',
        index: true,
    },
    description: {
        type: String,
        trim: true,
        deafult: '',
    },
    meta: {
        type: Object,
        default: {},
    },
    is_new: {
        type: Boolean,
        default: true,
        index: true,
    }
}, {timestamps: true});

Notification.index({sender: 1, createAt: -1})
Notification.index({receiver: 1, createAt: -1})
Notification.index({sender: 1, action: 1})
Notification.index({receiver: 1, action: 1})
Notification.index({post: 1, action: 1})

module.exports = Notification
