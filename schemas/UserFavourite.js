const {Schema} = require('mongoose')
const uniqueValidator = require('mongoose-unique-validator')

const User = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
        index: true,
    },
    post: {
        type: Schema.Types.ObjectId,
        ref: 'Post',
        required: true,
        index: true,
    }
}, {timestamps: true})

User.index({user: 1, post: 1})
User.index({user: 1, post: 1, createAt: -1})

module.exports = User
