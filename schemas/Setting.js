const {Schema} = require('mongoose')

const Setting = new Schema({

    key: {
        type: String,
        required: true,
        index: true,
    },

    value: {
        type: Schema.Types.Mixed,
        required: true,
    },

    is_deleted: {
        type: Boolean,
        default: false,
    },

})

module.exports = Setting
