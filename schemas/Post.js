const {Schema} = require('mongoose')
const mongooseLeanVirtuals = require('mongoose-lean-virtuals')

const Fee = new Schema({
    _id: false,

    unit: {
        type: String,
        required: true,
        // enum: ["month", "quater", "year"]
    },

    amount: {
        type: Number,
        index: true,
        required: true,
        min: 0,
    }
})

const Room = new Schema({
    _id: false,

    shared: {
        type: Boolean,
        required: true,
        index: true,
    },

    quantity: {
        type: Number,
        default: 1,
    },

    meta: {
        type: Object,
        default: {},
    }
})

const Utility = new Schema({
    _id: false,

    name: {
        type: String,
        trim: true,
        required: true,
        index: true,
    },

    shared: {
        type: Boolean,
        required: true,
        index: true,
    },

    quantity: {
        type: Number,
        default: 1,
    },

    meta: {
        type: Object,
        default: {},
    }
})

const Accommodation = new Schema({
    _id: false,

    address: {
        type: Schema.Types.ObjectId,
        ref: 'Address',
        required: true,
        index: true,
    },
    nears: {
        type: String,
        trim: true,
        default: '',
        index: 'text',
    },
    // nears: {
    //     type: [{
    //         type: Schema.Types.ObjectId,
    //         ref: 'Address',
    //     }],
    //     default: [],
    // },
    type: {
        type: String,
        required: true,
        enum: ["Phòng trọ", "Chung cư mini", "Nhà nguyên căn", "Chung cư nguyên căn"],
        index: true,
    },
    quantity: {
        type: Number,
        required: true,
        min: 1,
    },
    rent_fee: {
        type: Fee,
        require: true,
    },
    area: {
        type: Number,
        required: true,
        min: 0,
        index: true,
    },
    shared: {
        type: Boolean,
        default: false
    },
    bathroom: {
        type: Room,
        required: true,
    },
    kitchen: {
        type: Room,
        required: true,
        min: 0,
    },
    electrict_fee: {
        type: Fee,
        require: true,
    },
    water_fee: {
        type: Fee,
        require: true,
    },
    utilities: {
        type: [Utility],
        default: [],
    },
    images: {
        type: [String],
        default: [],
    },
})

const Contact = new Schema({
    _id: false,

    full_name: {
        type: String,
        trim: true,
        required: true,
    },

    phone: {
        type: String,
        trim: true,
        required: true,
    },
})

const Post = new Schema({
    author: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
        index: true,
    },
    contact: {
        type: Contact,
        required: true,
    },
    title: {
        type: String,
        trim: true,
        required: true,
        index: true,
    },
    description: {
        type: String,
        trim: true,
    },
    detail: {
        type: Accommodation,
        require: true,
    },
    expired_at: {
        type: Date,
        index: true,
    },
    check_by: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    },
    approved_at: {
        type: Date,
        index: true,
    },
    rejected_at: {
        type: Date,
        index: true,
    },
    rented_at: {
        type: Date,
        index: true,
    },
    views: {
        type: Number,
        default: 0,
        index: true,
    },
    likes: {
        type: Number,
        default: 0,
        index: true,
    },
    is_deleted: {
        type: Boolean,
        default: false,
    },
}, {timestamps: true})

Post.index({'detail.nears': 'text'})

Post.virtual('rates', {
    ref: 'Rate',
    localField: '_id',
    foreignField: 'post',
    match: {approved_at: {$exists: true}, is_deleted: {$ne: true}},
    populate: {
        path: 'user',
    }
})

Post.plugin(mongooseLeanVirtuals)

module.exports = Post
