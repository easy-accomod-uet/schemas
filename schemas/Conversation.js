const {Schema} = require('mongoose')

const Conversation = new Schema({
    user_one: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
        index: true,
    },
    user_two: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
        index: true,
    },
    meta: {
        type: Object,
        default: {},
    }
}, {timestamps: true})

Conversation.index({user_one: 1, user_two: 1})

module.exports = Conversation
