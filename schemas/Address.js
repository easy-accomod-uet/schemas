const {Schema} = require('mongoose')

const Address = new Schema({
    province: {
        type: String,
        trim: true,
        required: true,
        index: true,
    },
    district: {
        type: String,
        trim: true,
        required: true,
        index: true,
    },
    address: {
        type: String,
        trim: true,
        required: true,
    },
})

Address.index({'province': 1, 'district': 1})
Address.index({'province': 1, 'district': 1, 'address': 1})
// Address.index({'province': 'text'})
// Address.index({'district': 'text'})
Address.index({'address': 'text', 'province': 'text', 'district': 'text'})

module.exports = Address
