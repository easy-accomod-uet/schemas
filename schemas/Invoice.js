const {Schema} = require('mongoose')

const Invoice = new Schema({
    post: {
        type: Schema.Types.ObjectId,
        required: true,
        index: true,
    },

    user: {
        type: Schema.Types.ObjectId,
        required: true,
        index: true,
    },

    amount: {
        type: Number,
        default: 0,
    },

    note: {
        type: String,
        trim: true,
        default: '',
    },

    paid_at: {
        type: Date,
    },

    type: {
        type: String,
        trim: true,
        default: 'create-post',
    }

}, {timestamps: true})

module.exports = Invoice
