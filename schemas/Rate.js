const {Schema} = require('mongoose')

const Rate = new Schema({
    post: {
        type: Schema.Types.ObjectId,
        ref: 'Post',
        required: true,
        index: true,
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
        index: true,
    },
    approved_at: {
        type: Date,
    },
    rejected_at: {
        type: Date,
    },
    rate: {
        type: Number,
        required: true,
        min: 0.0,
        max: 5.0,
        index: true,
    },
    comment: {
        type: String,
        trim: true,
        default: '',
    },
    is_deleted: {
        type: Boolean,
        default: false,
    }

}, {timestamps: true})

module.exports = Rate
