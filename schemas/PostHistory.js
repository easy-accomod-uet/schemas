const {Schema} = require('mongoose')

const PostHistory = new Schema({
    post: {
        type: Schema.Types.ObjectId,
        ref: 'Post',
        required: true,
        index: true,
    },
    views: {
        type: Number,
        required: true,
    },
    likes: {
        type: Number,
        required: true,
    },
    meta: {
        type: Object,
        default: {},
    },
    created_at: {
        type: Date,
        default: Date.now(),
        index: true,
    }
})

module.exports = PostHistory
