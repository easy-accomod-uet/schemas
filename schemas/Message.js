const {Schema} = require('mongoose')

const Message = new Schema({
    conversation: {
        type: Schema.Types.ObjectId,
        ref: 'Conversation',
        required: true,
        index: true,
    },
    user: {
        type: Number,
        ref: 'User',
        required: true,
        index: true,
    },
    content: {
        type: String,
        required: true
    },
    meta: {
        type: Object,
        default: {},
    }
}, {timestamps: true})

Message.index({conversation: 1, createAt: -1})
Message.index({conversation: 1, user: 1})

module.exports = Message
